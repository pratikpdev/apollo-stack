<?php
namespace Chisel\Extensions;

class NewsCPT {
  /**
   * Run the actions
   */
  public function run() {
    add_action('init', $this->register_news_post_type());
    add_action('init', $this->register_news_taxonomy_category());
  }

  /**
  * Register News Post Type
  *
  * @access public
  * @return void
  */
  public function register_news_post_type() {
    register_post_type( 'news',
      array(
        'labels' => array(
          'name'          => __( 'news' ),
          'singular_name' => __( 'news' ),
          'all_items'     => __( 'All news' ),
          'add_new'       => __( 'Add new' ),
          'add_new_item'  => __( 'Add New news' ),
          'edit_item'     => __( 'Edit news' ),
		  'menu_name' 	  => __( 'News' ),
		  'name_admin_bar'=> __( 'News' ),
		  'new_item' 	  => __( 'New news' ),
		  'view_item' 	  => __( 'View news' ),
		  'search_items'  => __( 'Search news' ),
		  'not_found' 	  => __( 'No news found.' ),
        ),
		'menu_icon'	  		  => 'dashicons-schedule',
        'exclude_from_search' => false,
        'has_archive'         => true,
		'hierarchical' 		  => false,
        'public'              => true,
		'query_var'			  => true,
        'rewrite'             => true,
	//	'rewrite' 			  => array('slug' => 'news'),
        'show_in_nav_menus'   => false,
        'show_ui'             => true,
        'supports'            => array('title', 'editor', 'thumbnail')
      )
    );
  }

  /**
  * Register News Post Type
  *
  * @access public
  * @return void
  */
  public function register_news_taxonomy_category () {
    register_taxonomy('news-category', 'news', array(
      'hierarchical' 	=> true,
      'show_tagcloud' 	=> false,
      'labels' 			=> array(
        'name' 				=> __( 'Categories' ),
        'singular_name' 	=> __( 'Category' ),
        'search_items' 		=> __( 'Search Categories' ),
        'all_items' 		=> __( 'All Categories' ),
        'parent_item' 		=> __( 'Parent Category' ),
        'parent_item_colon' => __( 'Parent Category:' ),
        'edit_item' 		=> __( 'Edit Category' ),
        'update_item' 		=> __( 'Update Category' ),
        'add_new_item' 		=> __( 'Add New Category' ),
        'new_item_name' 	=> __( 'New Category Name' ),
        'menu_name' 		=> __( 'Categories' ),
      )
    ));
  }
}
?>