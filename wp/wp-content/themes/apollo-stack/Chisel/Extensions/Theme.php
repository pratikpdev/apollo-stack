<?php

namespace Chisel\Extensions;

/**
 * Class Theme
 * Use this class to extend theme functionality
 * @package Chisel\Extensions
 */
class Theme implements ChiselExtension {
	public function extend() {
		$this->addThemeSupports();
		$this->addThemeSettingsOptionInAdmin();
	}

	private function addThemeSupports() {
		add_theme_support( 'post-formats' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'menus' );
		add_theme_support( 'title-tag' );
	}
	
	private function addThemeSettingsOptionInAdmin() {
		add_action('acf/init', function () {
			if (function_exists('acf_add_options_page')) {
				acf_add_options_page(array(
					'page_title' => __('Chisel Theme Settings', 'chisel'),
					'menu_title' => __('Theme Settings', 'chisel'),
					'menu_slug' => 'theme-settings',
				));
			}
		});
	}
}
