/* eslint-disable no-param-reassign */

const creatorData = {
  chiselVersion: '1.0.0-alpha.12',
  app: {
    name: 'Apollo Stack',
    author: 'Apollo Stack',
    projectType: 'wp-with-fe',
    browsers: ['modern', 'edge18', 'ie11'],
    nameSlug: 'apollo-stack',
    hasJQuery: false,
  },
  wp: {
    title: 'Apollo Stack',
    url: 'http://dev.wp-demo.com/',
    adminUser: 'admin',
    adminEmail: 'crea8ivedev@gmail.com',
    tablePrefix: 'aps_',
  },
  wpPlugins: {
    plugins: [
      'Classic Editor',
      'WP Premium: Advanced Custom Fields Pro',
      'WP Sync DB',
      'WP Sync DB Media File Addon',
    ],
  },
};

const wp = {
  directoryName: 'wp',
  themeName: 'apollo-stack',
  url: 'http://dev.wp-demo.com/',
};

module.exports = {
  creatorData,

  wp,

  output: {
    base: `${wp.directoryName}/wp-content/themes/${wp.themeName}/dist`,
  },

  // To use React and hot reload for React components:
  // 1. Run `yarn add react-hot-loader @hot-loader/react-dom`
  // 3. Mark your root component as hot-exported as described on
  //    https://github.com/gaearon/react-hot-loader#getting-started (step 2)
  // 4. Uncomment line below
  // react: true,

  plugins: ['chisel-plugin-code-style', 'chisel-plugin-wordpress'],
};
